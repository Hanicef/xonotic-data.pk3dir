ast   "Asturian" "Asturianu" 73%
id_ID "Indonesian" "Bahasa Indonesia" 40%
de    "German" "Deutsch" 100%
de_CH "German (Switzerland)" "Deutsch (Schweiz)" 100%
en    "English" "English" 100%
en_AU "English (Australia)" "English (Australia)" 69%
es    "Spanish" "Español" 99%
fr    "French" "Français" 100%
ga    "Irish" "Irish" 29%
it    "Italian" "Italiano" 99%
la    "Latin" "Lingua Latina" 99%
hu    "Hungarian" "Magyar" 44%
nl    "Dutch" "Nederlands" 62%
pl    "Polish" "Polski" 79%
pt    "Portuguese" "Português" 77%
pt_BR "Portuguese (Brazil)" "Português (Brasil)" 100%
ro    "Romanian" "Romana" 67%
fi    "Finnish" "Suomi" 98%
sv    "Swedish" "Svenska" 99%
tr    "Turkish" "Türkçe" 55%
cs    "Czech" "Čeština" 30%
el    "Greek" "Ελληνική" 43%
be    "Belarusian" "Беларуская" 49%
bg    "Bulgarian" "Български" 69%
ru    "Russian" "Русский" 100%
sr    "Serbian" "Српски" 58%
uk    "Ukrainian" "Українська" 57%
zh_TW "Chinese (Taiwan)" "中文（正體字）" 99%
zh_CN "Chinese (China)" "中文（简体字）" 99%
zh_HK "Chinese (Hong Kong)" "中文（香港字）" 99%
ja_JP "Japanese" "日本語" 99%
ko    "Korean" "한국의" 35%
